#include "Actor.h"

Actor::Actor(): m_body(NULL)
{
}

Actor::~Actor()
{
}

void Actor::update()
{
	if(m_body)
	{
		btVector3 inpulse(0,0,0);
		if(m_input.left())
		{
			inpulse-=btVector3(1,0,0);
		}
		if(m_input.right())
		{
			inpulse+=btVector3(1,0,0);
		}
		if(m_input.forward())
		{
			inpulse-=btVector3(0,0,1);
		}
		if(m_input.back())
		{
			inpulse+=btVector3(0,0,1);
		}

		m_body->applyCentralImpulse(inpulse);
	}

	Node::update();
}

void Actor::render(Pipeline* pipeline, float alpha)
{
	pipeline->bind(glm::translate(glm::mat4(1.f),m_smooth.m_position)*glm::mat4_cast(m_smooth.m_rotation));
	PrimitiveHandle::get()->draw(PrimitiveHandle::CUBE_PRIMITIVE);
}