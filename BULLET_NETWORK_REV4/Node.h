#ifndef NODE_H
	#define NODE_H

#include "common.h"
#include "Pipeline.h"
#include "PrimitiveHandle.h"
#include "StateHistory.h"

class Node: public RakNet::Replica3, public btMotionState {
	public:
		friend class Client;
		friend class Server;

		Node();
		virtual ~Node();

		virtual void update();
		virtual void render(Pipeline* pipeline, float alpha=1.f)=0;

		// -- BULLET PHYSICS --
		virtual void getWorldTransform(btTransform &trans) const;
		virtual void setWorldTransform(const btTransform &trans);

		// -- RAKNET REPLICA --
		virtual RakNet::RakString GetName() const=0;
		virtual void WriteAllocationID(RakNet::Connection_RM3* destinationConnection, RakNet::BitStream* allocationIdBitstream) const;
		virtual void SerializeConstruction(RakNet::BitStream* constructionBitstream, RakNet::Connection_RM3* destinationConnection);
		virtual bool DeserializeConstruction(RakNet::BitStream* constructionBitstream, RakNet::Connection_RM3* sourceConnection);
		virtual void SerializeDestruction(RakNet::BitStream* destructionBitstream, RakNet::Connection_RM3* destinationConnection);
		virtual bool DeserializeDestruction(RakNet::BitStream* destructionBitstream, RakNet::Connection_RM3* sourceConnection);
		virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* serializeParameters);
		virtual void Deserialize(RakNet::DeserializeParameters *deserializeParameters);

		virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3 *destinationConnection, RakNet::ReplicaManager3 *replicaManager3) {
			return QueryConstruction_ServerConstruction(destinationConnection,m_isHosting);
		}

		virtual bool QueryRemoteConstruction(RakNet::Connection_RM3 *sourceConnection) {
			return QueryRemoteConstruction_ServerConstruction(sourceConnection,m_isHosting);
		}

		virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3 *destinationConnection) {
			return QuerySerialization_ClientSerializable(destinationConnection,m_isHosting);
		}

		virtual RakNet::RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3 *droppedConnection) const {
			return QueryActionOnPopConnection_Server(droppedConnection);
		}

		virtual void DeallocReplica(RakNet::Connection_RM3* sourceConnection) {
			delete this;
		}

	protected:
		struct Data
		{
			Data() {}
			Data(const glm::vec3& pos, const glm::fquat& rot): m_position(pos), m_rotation(rot)
			{
			}

			bool operator==(const Data& other) const {
				return bool(m_position==other.m_position&&m_rotation==other.m_rotation);
			}

			bool operator!=(const Data& other) const {
				return !(*this==other);
			}

			glm::vec3 m_position;
			glm::quat m_rotation;
		};

		static bool m_isHosting;
		StateHistory m_history;
		Data m_current;
		Data m_smooth;
};

#endif