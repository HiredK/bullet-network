#include "Client.h"
#include "Server.h"

int main(int argc, char** argv)
{
	std::string input;
	printf("/* %s */\n\n",APPLICATION_NAME);
	printf("Run application as (S)erver or (C)lient? ");
	std::cin>>input;

	Application* app=NULL;
	if(toupper(input[0])=='S')
	{
		app=new Server;
	}
	else
	{
		app=new Client;
	}

	return app->execute(argc,argv);
}