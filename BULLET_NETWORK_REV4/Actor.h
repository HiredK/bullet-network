#ifndef ACTOR_H
	#define ACTOR_H

#include "common.h"
#include "Node.h"
#include "Input.h"

class Actor: public Node {
	public:
		Actor();
		virtual ~Actor();

		virtual void update();
		virtual void render(Pipeline* pipeline, float alpha=1.f);

		inline void setClientGUID(RakNet::RakNetGUID guid) { m_clientGUID=guid; }
		inline RakNet::RakNetGUID getClientGUID() const { return m_clientGUID; }

		inline void setRigidBody(btRigidBody* body) { m_body=body; }
		inline btRigidBody* getRigidBody() const { return m_body; }

		inline void setInput(const Input& input) { m_input=input; }
		inline const Input& getInput(const Input& input) { return m_input; }

		// -- RAKNET REPLICA --
		virtual RakNet::RakString GetName() const { return RakNet::RakString("Actor"); }

	private:
		RakNet::RakNetGUID m_clientGUID;
		btRigidBody* m_body;
		Input m_input;
};

#endif