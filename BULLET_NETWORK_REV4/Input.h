#ifndef INPUT_H
	#define INPUT_H

#include "common.h"

class Input {
	public:
		enum Action {
			WALK_LEFT,
			WALK_RIGHT,
			WALK_FORWARD,
			WALK_BACK
		};

		Input();

		bool check();
		void action(Action type, bool state);

		inline bool left() const { return m_current.m_left; }
		inline bool right() const { return m_current.m_right; }
		inline bool forward() const { return m_current.m_forward; }
		inline bool back() const { return m_current.m_back; }

	private:
		struct Data {
			bool operator==(const Data &other) const
			{
				return bool(m_left==other.m_left&&
					m_right==other.m_right&&
					m_forward==other.m_forward&&
					m_back==other.m_back);
			}

			bool operator!=(const Data &other) const
			{
				return !(*this==other);
			}

			bool m_left;
			bool m_right;
			bool m_forward;
			bool m_back;
		};

		Data m_current;
		Data m_previous;
};

#endif