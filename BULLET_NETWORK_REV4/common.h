#ifndef COMMON_H
	#define COMMON_H

#define APPLICATION_NAME "Bullet Network"
#define APPLICATION_FIXED_STEP 0.01f

#define NETWORK_ENABLE_INTERPOLATION false
#define NETWORK_SERVER_INTERVAL_MS 80
#define NETWORK_SERVER_MAX_CLIENTS 32
#define NETWORK_SERVER_PORT 5000

// STD include
#include <iostream>

// SFML include
#define SFML_STATIC
#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

// GLEW include
#define GLEW_STATIC
#include <GL/glew.h>

// GLM include
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// RAKNET include
#include <RakNet/RakPeerInterface.h>
#include <RakNet/MessageIdentifiers.h>
#include <RakNet/NetworkIDManager.h>
#include <RakNet/ReplicaManager3.h>
#include <RakNet/GetTime.h>

// BULLET include
#include <Bullet/btBulletDynamicsCommon.h>

#endif