#include "Application.h"
#include "Time.h"

Application::Application(): m_peer(NULL), m_running(true)
{
}

bool Application::execute(int argc, char** argv)
{
	m_peer=m_peer=RakNet::RakPeerInterface::GetInstance();
	m_peer->AttachPlugin(&m_scene);
	if(onInit())
	{
		unsigned int t=0;
		float t_accumulator=0.0f;
		float t_absolute=0.0f;

		while(m_running)
		{
			float t_new=time();
			float t_delta=t_new-t_absolute;
			if(t_delta<=0.0f) continue;

			t_absolute=t_new;
			t_accumulator+=t_delta;
			while(t_accumulator>=APPLICATION_FIXED_STEP)
			{
				onUpdate();
				t_accumulator-=APPLICATION_FIXED_STEP;
				t++;
			}

			onRender(t_accumulator/APPLICATION_FIXED_STEP);
		} m_scene.destroy();
		
		m_peer->Shutdown(100,0);
		RakNet::RakPeerInterface::DestroyInstance(m_peer);
		return onCleanup();
	} return false;
}