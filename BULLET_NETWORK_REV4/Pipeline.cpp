#include "Pipeline.h"

Pipeline::Pipeline(): m_program(0), m_angle(glm::vec2(3.14f,0.f)), m_focused(true), m_initialize(false), m_reset(false)
{
}

bool Pipeline::initialize()
{
	glewExperimental=GL_TRUE;
	if(GLEW_OK==glewInit())
	{
		const char* VERT_SOURCE="uniform mat4 MVP; void main() { gl_Position=MVP*gl_Vertex; gl_TexCoord[0]=gl_MultiTexCoord0; }";
		const char* FRAG_SOURCE="void main() { gl_FragColor=vec4(1,0,0,1); }";

		unsigned int vertex=glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertex,1,&VERT_SOURCE,NULL);
		glCompileShader(vertex);

		unsigned int frag=glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(frag,1,&FRAG_SOURCE,NULL);
		glCompileShader(frag);

		if(validateShader(vertex)&&
			validateShader(frag))
		{
			m_program=glCreateProgram();
			glAttachShader(m_program,vertex);
			glAttachShader(m_program,frag);
			glLinkProgram(m_program);
			return true;
		}
	}

	return false;
}

void Pipeline::compute(sf::RenderWindow* window)
{
	if(sf::Mouse::isButtonPressed(sf::Mouse::Right)||!m_initialize)
	{
		if(m_initialize)
		{
			const float halfW=window->getSize().x*0.5f;
			const float halfH=window->getSize().y*0.5f;

			const float mX_old=m_mouse.x;
			const float mY_old=m_mouse.y;
			m_mouse.x=(sf::Mouse::getPosition(*window).x+mX_old)*0.5f;
			m_mouse.y=(sf::Mouse::getPosition(*window).y+mY_old)*0.5f;
			if(m_reset)
			{
				m_mouse.x=halfW;
				m_mouse.y=halfH;
				m_reset=false;
			}

			m_angle.x+=(halfW-m_mouse.x)*0.0018f;
			m_angle.y+=(halfH-m_mouse.y)*0.0018f;
			sf::Mouse::setPosition(sf::Vector2i(int(halfW),int(halfH)),*window);
			window->setMouseCursorVisible(false);
		}

		m_right=glm::vec3(sin(m_angle.x-3.14f/2.0f),0,cos(m_angle.x-3.14f/2.0f));
		m_direction=glm::vec3(cos(m_angle.y)*sin(m_angle.x),sin(m_angle.y),cos(m_angle.y)*cos(m_angle.x));
	} else if(!m_reset)
	{
		window->setMouseCursorVisible(true);
		m_reset=true;
	}

	m_pMatrix=glm::perspective(70.0f,float(window->getSize().x/window->getSize().y),1.0f,1500.f);
	m_vMatrix=glm::lookAt(m_position,m_position+m_direction,glm::cross(m_right,m_direction));
	m_initialize=true;
}

bool Pipeline::validateShader(unsigned int id) const
{
	int status;
	glGetShaderiv(id,GL_COMPILE_STATUS,&status);
	if(status==GL_FALSE)
	{
		int lenght;
		glGetShaderiv(id,GL_INFO_LOG_LENGTH,&lenght);

		char* log=new char[lenght+1];
		glGetShaderInfoLog(id,lenght,NULL,log);
		printf("%s\n",log);
		delete[] log;
		return false;
	} 
	
	return true;
}