#include "Client.h"

Client::Client(): m_connected(false), m_focused(true)
{
	Node::m_isHosting=false;
}

bool Client::onInit()
{
	RakNet::SocketDescriptor sd;
	sd.port=NETWORK_SERVER_PORT+1;
	while(RakNet::IRNS2_Berkley::IsPortInUse(sd.port,sd.hostAddress,sd.socketFamily,SOCK_DGRAM))
		sd.port++;

	printf("Starting client side application...\n");
	m_peer->SetMaximumIncomingConnections(1);
	if(m_peer->Startup(1,&sd,1)==RakNet::RAKNET_STARTED)
	{
		m_window.create(sf::VideoMode(1024,600),APPLICATION_NAME,sf::Style::Close);
		if(!m_pipeline.initialize())
		{
			printf("Unable to initialize pipeline!\n");
			sf::sleep(sf::seconds(1.f));
			return false;
		}


		const char* ip="localhost"; // ip here!
		if(m_peer->Connect(ip,NETWORK_SERVER_PORT,0,0,0)==RakNet::CONNECTION_ATTEMPT_STARTED)
		{
			printf("Sending request...\n\n");
			return true;
		}
	}

	return false;
}

void Client::onUpdate()
{
	for(RakNet::Packet* packet=m_peer->Receive(); packet; m_peer->DeallocatePacket(packet), packet=m_peer->Receive())
	{
		switch(packet->data[0])
		{
			case ID_CONNECTION_REQUEST_ACCEPTED:
				printf("ID_CONNECTION_REQUEST_ACCEPTED\n");
				m_host=packet->systemAddress;
				m_connected=true;
				break;

			case ID_CONNECTION_ATTEMPT_FAILED:
				printf("ID_CONNECTION_ATTEMPT_FAILED\n");
				sf::sleep(sf::seconds(1.f));
				m_running=false;
				return;
		}
	}

	if(m_connected)
	{
		sf::Event event;
		while(m_window.pollEvent(event))
		{
			switch(event.type)
			{
				case sf::Event::KeyPressed:
				case sf::Event::KeyReleased:
				{
					if(event.key.code==sf::Keyboard::A)
					{
						m_currentInput.action(Input::WALK_LEFT,bool(event.type==sf::Event::KeyPressed));
						break;
					}
					if(event.key.code==sf::Keyboard::D)
					{
						m_currentInput.action(Input::WALK_RIGHT,bool(event.type==sf::Event::KeyPressed));
						break;
					}
					if(event.key.code==sf::Keyboard::W)
					{
						m_currentInput.action(Input::WALK_FORWARD,bool(event.type==sf::Event::KeyPressed));
						break;
					}
					if(event.key.code==sf::Keyboard::S)
					{
						m_currentInput.action(Input::WALK_BACK,bool(event.type==sf::Event::KeyPressed));
						break;
					}

					break;
				}

				case sf::Event::GainedFocus:
					m_focused=true;
					break;

				case sf::Event::LostFocus:
					m_focused=false;
					break;

				case sf::Event::Closed:
					m_running=false;
					return;
			}
		}

		if(m_focused)
		{
			m_pipeline.compute(&m_window);
			if(m_currentInput.check())
			{
				RakNet::BitStream message;
				message.Write((RakNet::MessageID)ID_CLIENT_INPUT);
				message<<m_currentInput;

				m_peer->Send(&message,IMMEDIATE_PRIORITY,RELIABLE_ORDERED,0,m_host,false);
			}
		}

		m_scene.update();
	}
}

void Client::onRender(float alpha)
{
	if(m_connected)
	{
		m_window.clear();
		m_scene.render(&m_pipeline,alpha);
		m_window.display();
	}
}

bool Client::onCleanup()
{
	m_window.close();
	return true;
}