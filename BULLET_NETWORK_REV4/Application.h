#ifndef APPLICATION_H
	#define APPLICATION_H

#include "common.h"
#include "Scene.h"

class Application {
	public:
		enum Message {
			ID_CLIENT_INPUT=ID_USER_PACKET_ENUM+1
		};

		Application();
		bool execute(int argc, char** argv);

	protected:
		virtual bool onInit()=0;
		virtual void onUpdate()=0;
		virtual void onRender(float alpha=1.f)=0;
		virtual bool onCleanup()=0;

		RakNet::RakPeerInterface* m_peer;
		Scene m_scene;
		bool m_running;
};

#endif