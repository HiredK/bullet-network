#include "Scene.h"

Scene::Scene(): m_config(new btDefaultCollisionConfiguration), m_solver(new btSequentialImpulseConstraintSolver), m_broadphase(new btDbvtBroadphase)
{
	SetAutoSerializeInterval(NETWORK_SERVER_INTERVAL_MS);
	SetNetworkIDManager(&m_networkIDManager);

	m_dispatcher=new btCollisionDispatcher(m_config);
	m_world=new btDiscreteDynamicsWorld(m_dispatcher,m_broadphase,m_solver,m_config);
	m_world->setGravity(btVector3(0,-9.8f,0));

	{
		btTransform transform;
		transform.setIdentity();
		transform.setOrigin(btVector3(0,-10,0));
		btDefaultMotionState* state=new btDefaultMotionState(transform);
		m_world->addRigidBody(buildRigidBody(state,glm::vec3(500,1,500),0.f));
	}
}

void Scene::update()
{
	m_world->stepSimulation(APPLICATION_FIXED_STEP,0);
	for(unsigned int i=0; i<GetReplicaCount(); i++)
	{
		Node* node=(Node*)GetReplicaAtIndex(i);
		node->update();
	}
}

void Scene::render(Pipeline* pipeline, float alpha)
{
	for(unsigned int i=0; i<GetReplicaCount(); i++)
	{
		Node* node=(Node*)GetReplicaAtIndex(i);
		node->render(pipeline,alpha);
	}
}

btRigidBody* Scene::buildRigidBody(btMotionState* state, const glm::vec3& size, float mass)
{
	btVector3 localInertia(0,0,0);
	btCollisionShape* shape=new btBoxShape(btVector3(size.x,size.y,size.z));
	if(mass>0.f) 
	{
		shape->calculateLocalInertia(mass,localInertia);
	}
	
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass,state,shape,localInertia);
	btRigidBody* body=new btRigidBody(rbInfo);
	body->setActivationState(DISABLE_DEACTIVATION);
	m_world->addRigidBody(body);
	return body;
}

Actor* Scene::createActor(RakNet::RakNetGUID client)
{
	Actor* actor=new Actor;
	actor->setClientGUID(client);
	actor->setRigidBody(buildRigidBody(actor,glm::vec3(1),10.f));
	Reference(actor);
	return actor;
}

void Scene::destroy()
{
	delete m_config;
	m_config=NULL;

	delete m_solver;
	m_solver=NULL;

	delete m_broadphase;
	m_broadphase=NULL;

	delete m_dispatcher;
	m_dispatcher=NULL;

	//delete m_world;
	//m_world=NULL;
}

RakNet::Connection_RM3* Scene::AllocConnection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID rakNetGUID) const
{
	return new Connection(systemAddress,rakNetGUID);
}

void Scene::DeallocConnection(RakNet::Connection_RM3 *connection) const
{
	delete connection;
}