#ifndef PIPELINE_H
	#define PIPELINE_H

#include "common.h"

class Pipeline {
	public:
		Pipeline();

		bool initialize();
		void compute(sf::RenderWindow* window);
		inline void bind(const glm::mat4& model)
		{
			glUniformMatrix4fv(glGetUniformLocation(m_program,"MVP"),1,false,glm::value_ptr(m_pMatrix*m_vMatrix*model));
			glUseProgram(m_program);
		}

	private:
		bool validateShader(unsigned int id) const;

	protected:
		unsigned int m_program;
		glm::mat4 m_pMatrix;
		glm::mat4 m_vMatrix;

		glm::vec3 m_position;
		glm::vec3 m_direction;
		glm::vec3 m_right;

		glm::vec2 m_mouse;
		glm::vec2 m_angle;

		bool m_focused;
		bool m_initialize;
		bool m_reset;
};

#endif