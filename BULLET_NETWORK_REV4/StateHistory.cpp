#include "StateHistory.h"

StateHistory::StateHistory(RakNet::TimeMS maxWriteInterval, RakNet::TimeMS maxHistoryTime)
{
	m_writeInterval=maxWriteInterval;
	m_maxHistoryLength=maxHistoryTime/maxWriteInterval+1;
	m_history.ClearAndForceAllocation(m_maxHistoryLength+1,_FILE_AND_LINE_);
}

void StateHistory::write(const glm::vec3& pos, const glm::quat& rot, RakNet::TimeMS t)
{
	if(m_history.Size())
	{
		const StateCell& last=m_history.PeekTail();
		if(t-last.m_time>=m_writeInterval)
		{
			m_history.Push(StateCell(t,pos,rot),_FILE_AND_LINE_);
			if(m_history.Size()>m_maxHistoryLength)
				m_history.Pop();
		}
	}
	else
		m_history.Push(StateCell(t,pos,rot),_FILE_AND_LINE_);
}

void StateHistory::overwrite(const glm::vec3& pos, const glm::quat& rot, RakNet::TimeMS t)
{
	const int size=m_history.Size();
	if(size)
	{
		for(int i=size-1; i>=0; i--)
		{
			StateCell& cell=m_history[i];
			if(t>=cell.m_time)
			{
				if(i==size-1&&t-cell.m_time>=m_writeInterval)
				{
					m_history.Push(StateCell(t,pos,rot),_FILE_AND_LINE_);
					if(m_history.Size()>m_maxHistoryLength)
						m_history.Pop();

					return;
				}

				cell.m_time=t;
				cell.m_position=pos;
				cell.m_rotation=rot;
				return;
			}
		}
	}
	else
		m_history.Push(StateCell(t,pos,rot),_FILE_AND_LINE_);
}

StateHistory::ReadResult StateHistory::read(glm::vec3* pos, glm::quat* rot, RakNet::TimeMS when, RakNet::TimeMS t)
{
	const int size=m_history.Size();
	if(size==0) return VALUES_UNCHANGED;
	for(int i=size-1; i>=0; i--)
	{
		const StateCell& cell=m_history[i];
		if(when>=cell.m_time)
		{
			if(i==size-1)
			{
				if(t<=cell.m_time) return VALUES_UNCHANGED;
				float lerp=float((when-cell.m_time))/float((t-cell.m_time));
				if(pos) 
				{
					*pos=cell.m_position+(*pos-cell.m_position)*lerp;
				}
				if(rot)
				{
					*rot=glm::slerp(cell.m_rotation,*rot,lerp);
				}
			} 
			else
			{
				const StateCell& next=m_history[i+1];
				float lerp=float(when-cell.m_time)/float(next.m_time-cell.m_time);
				if(pos)
				{
					*pos=cell.m_position+(next.m_position-cell.m_position)*lerp;
				}
				if(rot)
				{
					*rot=glm::slerp(cell.m_rotation,next.m_rotation,lerp);
				}
			} 
			
			return INTERPOLATED;
		}
	}

	const StateCell& last=m_history.Peek();
	*pos=last.m_position;
	*rot=last.m_rotation;
	return READ_OLDEST;
}

void StateHistory::clear()
{
	m_history.Clear(_FILE_AND_LINE_);
}