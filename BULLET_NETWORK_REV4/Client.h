#ifndef CLIENT_H
	#define CLIENT_H

#include "common.h"
#include "Application.h"
#include "Pipeline.h"

class Client: public Application {
	public:
		Client();

	protected:
		virtual bool onInit();
		virtual void onUpdate();
		virtual void onRender(float alpha=1.f);
		virtual bool onCleanup();

	private:
		RakNet::SystemAddress m_host;
		sf::RenderWindow m_window;
		Input m_currentInput;
		Pipeline m_pipeline;
		bool m_connected;
		bool m_focused;
};

#endif