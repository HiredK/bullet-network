#include "PrimitiveHandle.h"

PrimitiveHandle* PrimitiveHandle::m_instance = NULL;
PrimitiveHandle* PrimitiveHandle::get()
{
	if(m_instance==NULL) m_instance=new PrimitiveHandle();
	return m_instance;
}

PrimitiveHandle::PrimitiveHandle()
{
	for(int type=0; type<PRIMITIVE_TYPE_NUM; type++)
	{
		m_vbo[type][VERTEX]=0;
		m_vbo[type][COORD]=0;
		m_vbo[type][NORMAL]=0;
		m_vbo[type][INDICES]=0;
	}
}

PrimitiveHandle::~PrimitiveHandle()
{
	for(int type=0; type<PRIMITIVE_TYPE_NUM; type++)
	{
		if(m_vbo[type][VERTEX]) glDeleteBuffers(1,&m_vbo[type][VERTEX]);
		if(m_vbo[type][COORD]) glDeleteBuffers(1,&m_vbo[type][COORD]);
		if(m_vbo[type][NORMAL]) glDeleteBuffers(1,&m_vbo[type][NORMAL]);
		if(m_vbo[type][INDICES]) glDeleteBuffers(1,&m_vbo[type][INDICES]);
	}
}

void PrimitiveHandle::draw(PrimitiveType type)
{
	glEnable(GL_CULL_FACE);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	unsigned int ptype=GL_TRIANGLES;
	int count=0;

	switch(type)
	{
		case CUBE_PRIMITIVE:
		{
			if(!m_vbo[CUBE_PRIMITIVE][VERTEX])
			{
				glm::vec3 vertices[24] =
				{
					// front
					glm::vec3(-1,-1, 1),
					glm::vec3( 1,-1, 1),
					glm::vec3( 1, 1, 1),
					glm::vec3(-1, 1, 1),

					// top
					glm::vec3(-1, 1, 1),
					glm::vec3( 1, 1, 1),
					glm::vec3( 1, 1,-1),
					glm::vec3(-1, 1,-1),

					// back
					glm::vec3( 1,-1,-1),
					glm::vec3(-1,-1,-1),
					glm::vec3(-1, 1,-1),
					glm::vec3( 1, 1,-1),

					// bottom
					glm::vec3(-1,-1,-1),
					glm::vec3( 1,-1,-1),
					glm::vec3( 1,-1, 1),
					glm::vec3(-1,-1, 1),

					// left
					glm::vec3(-1,-1,-1),
					glm::vec3(-1,-1, 1),
					glm::vec3(-1, 1, 1),
					glm::vec3(-1, 1,-1),

					// right
					glm::vec3( 1,-1, 1),
					glm::vec3( 1,-1,-1),
					glm::vec3( 1, 1,-1),
					glm::vec3( 1, 1, 1)
				};

				// generate cube vertex buffer
				glGenBuffers(1,&m_vbo[CUBE_PRIMITIVE][VERTEX]);
				glBindBuffer(GL_ARRAY_BUFFER,m_vbo[CUBE_PRIMITIVE][VERTEX]);
				glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER,0);
			}

			if(!m_vbo[CUBE_PRIMITIVE][COORD])
			{
				glm::vec2 coord[24] =
				{
					// front
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1),

					// top
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1),

					// back
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1),

					// bottom
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1),

					// left
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1),

					// right
					glm::vec2(0,0),
					glm::vec2(1,0),
					glm::vec2(1,1),
					glm::vec2(0,1)
				};

				// generate cube coord buffer
				glGenBuffers(1,&m_vbo[CUBE_PRIMITIVE][COORD]);
				glBindBuffer(GL_ARRAY_BUFFER,m_vbo[CUBE_PRIMITIVE][COORD]);
				glBufferData(GL_ARRAY_BUFFER,sizeof(coord),coord,GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER,0);
			}

			if(!m_vbo[CUBE_PRIMITIVE][NORMAL])
			{
				glm::vec3 normal[24] =
				{
					// front
					glm::vec3( 0, 0, 1),
					glm::vec3( 0, 0, 1),
					glm::vec3( 0, 0, 1),
					glm::vec3( 0, 0, 1),

					// top
					glm::vec3( 0, 1, 0),
					glm::vec3( 0, 1, 0),
					glm::vec3( 0, 1, 0),
					glm::vec3( 0, 1, 0),

					// back
					glm::vec3( 0, 0,-1),
					glm::vec3( 0, 0,-1),
					glm::vec3( 0, 0,-1),
					glm::vec3( 0, 0,-1),

					// bottom
					glm::vec3( 0,-1, 0),
					glm::vec3( 0,-1, 0),
					glm::vec3( 0,-1, 0),
					glm::vec3( 0,-1, 0),

					// left
					glm::vec3(-1, 0, 0),
					glm::vec3(-1, 0, 0),
					glm::vec3(-1, 0, 0),
					glm::vec3(-1, 0, 0),

					// right
					glm::vec3( 1, 0, 0),
					glm::vec3( 1, 0, 0),
					glm::vec3( 1, 0, 0),
					glm::vec3( 1, 0, 0),
				};

				// generate cube normal buffer
				glGenBuffers(1,&m_vbo[CUBE_PRIMITIVE][NORMAL]);
				glBindBuffer(GL_ARRAY_BUFFER,m_vbo[CUBE_PRIMITIVE][NORMAL]);
				glBufferData(GL_ARRAY_BUFFER,sizeof(normal),normal,GL_DYNAMIC_DRAW);
				glBindBuffer(GL_ARRAY_BUFFER,0);
			}

			if(!m_vbo[CUBE_PRIMITIVE][INDICES])
			{
				unsigned int indices[36] =
				{
					 0,1,2,
					 2,3,0,
					 4,5,6,
					 6,7,4,
					 8,9,10,
					10,11,8,
					12,13,14,
					14,15,12,
					16,17,18,
					18,19,16,
					20,21,22,
					22,23,20,
				};

				// generate cube element buffer
				glGenBuffers(1,&m_vbo[CUBE_PRIMITIVE][INDICES]);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_vbo[CUBE_PRIMITIVE][INDICES]);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER,sizeof(indices),indices,GL_STATIC_DRAW);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
			}

			// assign to cube
			count=36;
			break;
		}
	}

	if(m_vbo[type][VERTEX])
	{
		// bind vertex buffer
		glBindBuffer(GL_ARRAY_BUFFER,m_vbo[type][VERTEX]);
		glVertexPointer(3,GL_FLOAT,sizeof(glm::vec3),(void*)0);
	}

	if(m_vbo[type][COORD])
	{
		// bind coord buffer
		glBindBuffer(GL_ARRAY_BUFFER,m_vbo[type][COORD]);
		glTexCoordPointer(2,GL_FLOAT,sizeof(glm::vec2),(void*)0);
	}

	if(m_vbo[type][NORMAL])
	{
		// bind normal buffer
		glBindBuffer(GL_ARRAY_BUFFER,m_vbo[type][NORMAL]);
		glNormalPointer(GL_FLOAT,sizeof(glm::vec3),(void*)0);
	}

	if(m_vbo[type][INDICES])
	{
		// bind element buffer
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,m_vbo[type][INDICES]);
	}

	// draw primitive
	glDrawElements(ptype,count,GL_UNSIGNED_INT,(void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,0);
	glBindBuffer(GL_ARRAY_BUFFER,0);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisable(GL_CULL_FACE);
}