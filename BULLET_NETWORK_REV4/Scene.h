#ifndef SCENE_H
	#define SCENE_H

#include "common.h"
#include "Actor.h"

class Scene: public RakNet::ReplicaManager3 {
	public:
		Scene();

		void update();
		void render(Pipeline* pipeline, float alpha=1.f);
		btRigidBody* buildRigidBody(btMotionState* state, const glm::vec3& size, float mass);
		Actor* createActor(RakNet::RakNetGUID client);
		void destroy();

		// -- RAKNET REPLICA --
		virtual RakNet::Connection_RM3* AllocConnection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID rakNetGUID) const;
		virtual void DeallocConnection(RakNet::Connection_RM3 *connection) const;

	protected:
		class Connection: public RakNet::Connection_RM3 {
			public:
				Connection(const RakNet::SystemAddress &systemAddress, RakNet::RakNetGUID guid): Connection_RM3(systemAddress,guid) {}
				virtual ~Connection() {}

				virtual RakNet::Replica3 *AllocReplica(RakNet::BitStream *allocationId, RakNet::ReplicaManager3 *replicaManager3) 
				{
					RakNet::RakString type;
					allocationId->Read(type);
					if(type=="Actor")
					{
						return new Actor;
					} return NULL;
				}
		};

		RakNet::NetworkIDManager m_networkIDManager;

		// -- BULLET PHYSICS --
		btDefaultCollisionConfiguration* m_config;
		btSequentialImpulseConstraintSolver* m_solver;
		btBroadphaseInterface* m_broadphase;
		btCollisionDispatcher* m_dispatcher;
		btDiscreteDynamicsWorld* m_world;
};

#endif