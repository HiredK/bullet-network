#ifndef TIME_H
	#define TIME_H

#include "windows.h"

static float time()
{
    static __int64 S=0;
    static __int64 F=0;
    if(S==0)
    {
        QueryPerformanceCounter((LARGE_INTEGER*)&S);
        QueryPerformanceFrequency((LARGE_INTEGER*)&F);
        return 0.0f;
    }

    __int64 counter=0;
    QueryPerformanceCounter((LARGE_INTEGER*)&counter);
    return (float) ((counter-S)/double(F));
}

#endif