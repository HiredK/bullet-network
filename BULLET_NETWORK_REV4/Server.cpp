#include "Server.h"

Server::Server()
{
	Node::m_isHosting=true;
}

bool Server::onInit()
{
	RakNet::SocketDescriptor sd;
	sd.port=NETWORK_SERVER_PORT;

	printf("Starting server side application...\n");
	m_peer->SetMaximumIncomingConnections(NETWORK_SERVER_MAX_CLIENTS);
	if(m_peer->Startup(NETWORK_SERVER_MAX_CLIENTS,&sd,1)==RakNet::RAKNET_STARTED)
	{
		printf("Running!\n\n");
		return true;
	}

	return false;
}

void Server::onUpdate()
{
	for(RakNet::Packet* packet=m_peer->Receive(); packet; m_peer->DeallocatePacket(packet), packet=m_peer->Receive())
	{
		switch(packet->data[0])
		{
			case ID_CLIENT_INPUT:
				receiveClientInput(packet);
				break;

			case ID_NEW_INCOMING_CONNECTION:
				printf("ID_NEW_INCOMING_CONNECTION\n");
				m_clients.push_back(m_scene.createActor(packet->guid));
				break;

			case ID_DISCONNECTION_NOTIFICATION:
				printf("ID_DISCONNECTION_NOTIFICATION\n");
				std::vector<Actor*>::iterator it=m_clients.begin();
				while(it!=m_clients.end())
				{
					if((*it)->getClientGUID()==packet->guid)
					{
						it=m_clients.erase(it);
					} else it++;
				} break;
		}
	}

	m_scene.update();
}

void Server::receiveClientInput(RakNet::Packet* packet)
{
	for(unsigned int i=0; i<m_clients.size(); i++)
	{
		if(m_clients[i]->getClientGUID()==packet->guid)
		{
			Input input;
			RakNet::BitStream message(packet->data,packet->length,false);
			message.IgnoreBytes(sizeof(RakNet::MessageID));
			message>>input;

			m_clients[i]->setInput(input);
			return;
		}
	}
}

void Server::onRender(float alpha)
{
}

bool Server::onCleanup()
{
	m_clients.clear();
	return true;
}