#include "Input.h"

Input::Input()
{
}

bool Input::check()
{
	if(m_current!=m_previous)
	{
		m_previous=m_current;
		return true;
	}

	return false;
}

void Input::action(Action type, bool state)
{
	switch(type)
	{
		case WALK_LEFT:
			m_current.m_left=state;
			break;
		case WALK_RIGHT:
			m_current.m_right=state;
			break;
		case WALK_FORWARD:
			m_current.m_forward=state;
			break;
		case WALK_BACK:
			m_current.m_back=state;
			break;
	}
}

namespace RakNet
{
	RakNet::BitStream& operator<<(RakNet::BitStream& out, Input& in)
	{
		out.Write(in.left());
		out.Write(in.right());
		out.Write(in.forward());
		out.Write(in.back());
		return out;
	}

	RakNet::BitStream& operator>>(RakNet::BitStream& in, Input& out)
	{
		bool left;
		if(in.Read(left)) 
		{
			out.action(Input::WALK_LEFT,left);
		}

		bool right;
		if(in.Read(right)) 
		{
			out.action(Input::WALK_RIGHT,right);
		}

		bool forward;
		if(in.Read(forward)) 
		{
			out.action(Input::WALK_FORWARD,forward);
		}

		bool back;
		if(in.Read(back)) 
		{
			out.action(Input::WALK_BACK,back);
		}
	}
}