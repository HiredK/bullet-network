#ifndef STATEHISTORY_H
	#define STATEHISTORY_H

#include "common.h"

// RAKNET queue data structure
#include "RakNet/RakMemoryOverride.h"
#include "RakNet/DS_Queue.h"

class StateHistory {
	public:
		enum ReadResult
		{
			READ_OLDEST,
			VALUES_UNCHANGED,
			INTERPOLATED
		};

		StateHistory(RakNet::TimeMS maxWriteInterval, RakNet::TimeMS maxHistoryTime);

		void write(const glm::vec3& pos, const glm::quat& rot, RakNet::TimeMS t);
		void overwrite(const glm::vec3& pos, const glm::quat& rot,  RakNet::TimeMS t);
		ReadResult read(glm::vec3* pos, glm::quat* rot, RakNet::TimeMS when, RakNet::TimeMS t);
		void clear();

	protected:	
		struct StateCell
		{
			StateCell() {}
			StateCell(RakNet::TimeMS time, const glm::vec3& pos, const glm::quat& rot): m_time(time), m_position(pos), m_rotation(rot)
			{
			}

			RakNet::TimeMS m_time;
			glm::vec3 m_position;
			glm::quat m_rotation;
		};

		DataStructures::Queue<StateCell> m_history;
		unsigned int m_maxHistoryLength;
		unsigned int m_writeInterval;
};

#endif