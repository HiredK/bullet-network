#include "Node.h"

bool Node::m_isHosting=false;

Node::Node(): m_history(10,1000)
{
}

Node::~Node()
{
	m_history.clear();
}

void Node::update()
{
	m_smooth=m_current;
	if(NETWORK_ENABLE_INTERPOLATION)
	{
		m_history.read(&m_smooth.m_position,&m_smooth.m_rotation,RakNet::GetTimeMS()-NETWORK_SERVER_INTERVAL_MS,RakNet::GetTimeMS());
	}
}

void Node::getWorldTransform(btTransform &trans) const
{
}

void Node::setWorldTransform(const btTransform &trans)
{
	if(m_isHosting)
	{
		m_current.m_position.x=trans.getOrigin().x();
		m_current.m_position.y=trans.getOrigin().y();
		m_current.m_position.z=trans.getOrigin().z();

		m_current.m_rotation.x=trans.getRotation().x();
		m_current.m_rotation.y=trans.getRotation().y();
		m_current.m_rotation.z=trans.getRotation().z();
		m_current.m_rotation.w=trans.getRotation().w();
	}
}

void Node::WriteAllocationID(RakNet::Connection_RM3* destinationConnection, RakNet::BitStream* allocationIdBitstream) const
{
	allocationIdBitstream->Write(GetName());
}

void Node::SerializeConstruction(RakNet::BitStream* constructionBitstream, RakNet::Connection_RM3* destinationConnection)
{
	constructionBitstream->Write(m_current.m_position);
	constructionBitstream->Write(m_current.m_rotation);
}

bool Node::DeserializeConstruction(RakNet::BitStream* constructionBitstream, RakNet::Connection_RM3* sourceConnection)
{
	constructionBitstream->Read(m_current.m_position);
	constructionBitstream->Read(m_current.m_rotation);
	return true;
}

void Node::SerializeDestruction(RakNet::BitStream* destructionBitstream, RakNet::Connection_RM3* destinationConnection)
{
}

bool Node::DeserializeDestruction(RakNet::BitStream* destructionBitstream, RakNet::Connection_RM3* sourceConnection)
{
	return true;
}

RakNet::RM3SerializationResult Node::Serialize(RakNet::SerializeParameters* serializeParameters)
{
	serializeParameters->outputBitstream[0].Write(m_current.m_position);
	serializeParameters->outputBitstream[0].Write(m_current.m_rotation);
	return RakNet::RM3SR_BROADCAST_IDENTICALLY;
}

void Node::Deserialize(RakNet::DeserializeParameters *deserializeParameters)
{
	deserializeParameters->serializationBitstream[0].Read(m_current.m_position);
	deserializeParameters->serializationBitstream[0].Read(m_current.m_rotation);
	m_history.write(m_current.m_position,m_current.m_rotation,RakNet::GetTimeMS());
}