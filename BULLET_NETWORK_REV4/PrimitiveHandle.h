#ifndef PRIMITIVEHANDLE_H
    #define PRIMITIVEHANDLE_H

#include "common.h"

class PrimitiveHandle {
	public:
		enum PrimitiveType {
			CUBE_PRIMITIVE,
			PRIMITIVE_TYPE_NUM
		};

		enum PrimitiveAttribute {
			VERTEX,
			COORD,
			NORMAL,
			INDICES,
			PRIMITIVE_ATTRIBUTE_NUM
		};

		static PrimitiveHandle* get();
		~PrimitiveHandle();

		void draw(PrimitiveType type);

	protected:
		PrimitiveHandle();

	private:
		static PrimitiveHandle* m_instance;
		unsigned int m_vbo[PRIMITIVE_TYPE_NUM][PRIMITIVE_ATTRIBUTE_NUM];
};

#endif