#ifndef SERVER_H
	#define SERVER_H

#include "common.h"
#include "Application.h"

class Server: public Application {
	public:
		Server();

	protected:
		virtual bool onInit();
		virtual void onUpdate();
		virtual void onRender(float alpha=1.f);
		virtual bool onCleanup();

	private:
		void receiveClientInput(RakNet::Packet* packet);
		std::vector<Actor*> m_clients;
};

#endif